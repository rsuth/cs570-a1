Assignment: Assignment 1

**Available for testing in the class account of Richard Sutherland (cssc1176).**

Class: CS570 - Summer 2018

Programmers: Richard Sutherland (cssc1176) , Hamad Ahmad (cssc1177), Casey Guo (cssc1178)

Filename: README.txt

File Manifest: 
    a1.c:       main source code
    a1.h:       header file
    makefile:   the makefile
    README:     this Readme file

Compile instructions: in project directory, run the "make" command.

Operating instructions: "make run" or after compile "./player"

Design Decisions:   A struct was created to hold the data for each suit of card, because the pthread_create() 
                    function can only pass one argument to the designated function to run in the thread. So we 
                    passed a pointer to the struct containing that card's data. This allowed us to generalize the
                    card printing function to print any type of card.

Lessons Learned:    We learned the real-world processes by which POSIX threads are created and managed, and how to 
                    use sempaphores to manage threads.