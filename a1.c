// Assignment 1 - CS570 - SDSU Summer 2018
// Richard Sutherland (cssc1176) , Hamad Ahmad (cssc1177), Casey Guo (cssc1178)
// Filename: a1.c

#include "a1.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>

sem_t FLAG; // create semaphore in the global scope
FILE *f;    // file pointer in global scope

struct thread_data
{
    int thread_id;
    char *suit;
    long double delay;
};

// the function that will run in each thread, which writes the card suit
// and number to the file.
int print_cards(void *threadData)
{
    struct thread_data *my_data = (struct thread_data *)threadData;

    int my_id = my_data->thread_id;        // the thread id (1-4)
    char *my_suit = my_data->suit;         // the suit of card (Diamond, Club, etc..)
    long double my_delay = my_data->delay; // the number of microseconds to delay
    char rank;                             // (A, 2-10, J, Q, K)

    for (int i = 1; i <= 13; i++)
    {
        // wait the designated amount of time before the next write
        nanosleep((const struct timespec[]){{0, my_delay}}, NULL);

        // get the FLAG semaphore if it can.
        sem_wait(&FLAG);
        f = fopen("STACK.txt", "a");
        if (f == NULL)
        {
            printf("Error opening STACK.TXT\n");
            return -1;
        }

        if (i == 1)
        {
            rank = 'A';
        }
        else if (i == 11)
        {
            rank = 'J';
        }
        else if (i == 12)
        {
            rank = 'Q';
        }
        else if (i == 13)
        {
            rank = 'K';
        }
        else
        {
            rank = i + 48;
        }

        if (i != 10)
        {
            fprintf(f, "%s %c\r\n", my_suit, rank);
        }
        else
        {
            fprintf(f, "%s %d\r\n", my_suit, i);
        }

        printf("Thread %d is running\n", my_id);

        fclose(f);
        sem_post(&FLAG); // release the FLAG semaphore
    }

    pthread_exit(0); // thread is done

    return 0;
}

// runs the four threads, returns 0 if success, -1 if error.
int run_threads(void)
{

    // initialize the pthread pointers
    pthread_t t_1;
    pthread_t t_2;
    pthread_t t_3;
    pthread_t t_4;

    // initialize struct for each suit
    struct thread_data diamonds = {.thread_id = 1, .suit = "Diamond", .delay = 125000000};
    struct thread_data clubs = {.thread_id = 2, .suit = "Club", .delay = 250000000};
    struct thread_data hearts = {.thread_id = 3, .suit = "Heart", .delay = 500000000};
    struct thread_data spades = {.thread_id = 4, .suit = "Spade", .delay = 750000000};

    // create the threads
    int err[4];
    err[0] = pthread_create(&t_1, NULL, (void *)print_cards, (void *)&diamonds);
    err[1] = pthread_create(&t_2, NULL, (void *)print_cards, (void *)&clubs);
    err[2] = pthread_create(&t_3, NULL, (void *)print_cards, (void *)&hearts);
    err[3] = pthread_create(&t_4, NULL, (void *)print_cards, (void *)&spades);
    // check for errors
    for (int i = 0; i < 4; i++)
    {
        if (err[i] != 0)
        {
            printf("Error creating thread %d", (i + 1));
            return -1;
        }
    }
    // wait for the threads to finish
    pthread_join(t_1, NULL);
    pthread_join(t_2, NULL);
    pthread_join(t_3, NULL);
    pthread_join(t_4, NULL);
    return 0;
}

// writes PID to STACK.txt. Returns 0 if successful, -1 if there was an error.
int write_pid_to_file(void)
{
    f = fopen("STACK.txt", "w+");
    if (f == NULL)
    {
        printf("Error opening STACK.TXT\n");
        return -1;
    }
    int pid = getpid();
    fprintf(f, "%d\r\n", pid);
    fclose(f);
    return 0;
}

int main()
{
    // write the PID to STACK.txt
    if (write_pid_to_file() != 0)
    {
        printf("Error writing PID to STACK.txt");
        return -1;
    }
    // initialize the semaphore so that only one thread can write to the file at a time
    sem_init(&FLAG, 0, 1);
    // run the threads
    if (run_threads() != 0)
    {
        printf("Error running threads");
        return -1;
    }
    // done, destroy the semaphore
    sem_destroy(&FLAG);
    // print a friendly message to console
    printf("Finished!\n");
    return 0;
}
