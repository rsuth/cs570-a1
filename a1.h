// Assignment 1 - CS570 - SDSU Summer 2018
// Richard Sutherland (cssc1176) , Hamad Ahmad (cssc1177), Casey Guo (cssc1178)
// Filename: a1.h

// Card printing function - accepts a struct with card data
// returns 0 if success, -1 on error.
int print_cards(void *threadData);

// creates and runs the 4 threads
// returns 0 on success, -1 on error.
int run_threads(void);

// writes the PID to the STACK.txt file.
// returns 0 for success, -1 on error.
int write_pid_to_file(void);