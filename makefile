# Assignment 1 - CS570 - SDSU Summer 2018
# Richard Sutherland (cssc1176) , Hamad Ahmad (cssc1177), Casey Guo (cssc1178)
# Filename: makefile

CFLAGS = -Wall -std=gnu99 -pthread
PROGRAM_NAME = player

$(PROGRAM_NAME):
	gcc $(CFLAGS) -o $(PROGRAM_NAME) a1.c
clean:
	rm $(PROGRAM_NAME) STACK.txt
run:
	@$(MAKE) && ./$(PROGRAM_NAME)
